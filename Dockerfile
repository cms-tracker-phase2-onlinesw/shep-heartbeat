FROM alpine

RUN apk add python3 py3-requests

ADD heartbeat.py /heartbeat.py
COPY entry.sh /entry.sh
RUN chmod 755 /entry.sh

CMD ["/entry.sh"]
