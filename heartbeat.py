import requests
import os

def main():
    # print("Pinging boards...")
    boards = requests.get("http://{}/api/v1/boards".format(os.getenv('SHEP_HOSTNAME'))).json()['results']
    for board in boards:
        try:
            r = requests.get("http://{}/devices".format(board['ip']))
            if (r.status_code == 200 and board['online'] == False):
                requests.patch("http://{}/api/v1/boards/{}".format(os.getenv('SHEP_HOSTNAME'), board['uuid']), data={'online': True})
        except requests.exceptions.ConnectionError:
            requests.patch("http://{}/api/v1/boards/{}".format(os.getenv('SHEP_HOSTNAME'), board['uuid']), data={'online': False})
if __name__ == '__main__':
    main()
